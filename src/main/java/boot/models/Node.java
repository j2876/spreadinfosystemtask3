package boot.models;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.*;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
@Table(name = "node")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Node {
    @Transient
    public static Node empty = new Node();

    @Id
    @Column(name = "id")
    long id;

    @Column(name = "version")
    int version;

    @Column(name = "cdate")
    Date date;

    @ToString.Exclude
    @ManyToOne(
            optional = false,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}
    )
    @JoinColumn(name = "uid")
    User user;

    @Column(name = "changeset")
    long changeSet;

    @Column(name = "lat")
    double lat;

    @Column(name = "lon")
    double lon;

    @OneToMany(
            mappedBy = "node",
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}
    )
    Collection<Tag> tags;

    public void setTags(final Collection<Tag> tags) {
        tags.forEach(it -> it.setNode(this));
        this.tags = tags;
    }

    public static Node from(final boot.osm.Node node) {
        var result = Node.builder()
                .id(node.getId().longValue())
                .version(node.getVersion().intValue())
                .date(node.getTimestamp().toGregorianCalendar().getTime())
                .user(User.getUserFromNode(node))
                .changeSet(node.getChangeset().longValue())
                .lat(node.getLat())
                .lon(node.getLon())
                .build();

        result.setTags(Tag.getAllTagsFromNode(node));

        return result;
    }
}

