package boot.models;

import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;
import java.util.Collection;
import java.util.stream.Collectors;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tag")
public class Tag {
    @Column(name = "k")
    String key;

    @Column(name = "v")
    String value;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @ToString.Exclude
    @ManyToOne(optional = false)
    @JoinColumn(name = "nodeid")
    Node node;

    public static  Tag getTagFromNode(final boot.osm.Tag tag) {
        return Tag.builder()
                .node(Node.empty)
                .key(tag.getK())
                .value(tag.getV())
                .build();
    }

    public static Collection<Tag> getAllTagsFromNode(final @NotNull boot.osm.Node node) {
        return node.getTag().stream()
                .map(Tag::getTagFromNode)
                .collect(Collectors.toSet());
    }

}
