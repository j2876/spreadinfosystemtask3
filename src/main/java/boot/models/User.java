package boot.models;

import lombok.*;
import javax.persistence.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "osm_user")
public class User {
    @Transient
    public static User emptyUser = new User();

    @Id
    @Column(name = "uid")
    long id;

    @Column(name = "username")
    String name;

    public static User getUserFromNode(final boot.osm.Node node) {
        return User.builder()
                .id(node.getUid().longValue())
                .name(node.getUser())
                .build();
    }
}

