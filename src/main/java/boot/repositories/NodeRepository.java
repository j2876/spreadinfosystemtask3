package boot.repositories;

import boot.models.Node;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface NodeRepository extends JpaRepository<Node, Long> {
    @Query(
            value =
                    """
                    with distances(
                        id,
                        other_distance_1,
                        other_distance_2,
                        origin_distance_1,
                        origin_distance_2
                    ) as (
                        select
                            id,
                            public.earth_distance(public.ll_to_earth(node.lat, node.lon), public.ll_to_earth(0, 0)),
                            public.earth_distance(public.ll_to_earth(node.lat, node.lon), public.ll_to_earth(0, 90)),
                            public.earth_distance(public.ll_to_earth(0, 0), public.ll_to_earth(:lat, :lon)),
                            public.earth_distance(public.ll_to_earth(0, 90), public.ll_to_earth(:lat, :lon))
                        from
                            public.node node
                    ),
                    points(
                        id,
                        distance
                    ) as (
                        select
                            other.id,
                            public.earth_distance(public.ll_to_earth(other.lat, other.lon), public.ll_to_earth(:lat, :lon))
                        from
                            public.node other
                            join distances on other.id = distances.id
                        where
                            distances.other_distance_1
                                between distances.origin_distance_1 - :radius
                                and distances.origin_distance_1 + :radius
                                and 
                                distances.other_distance_2
                                between distances.origin_distance_2 - :radius
                                and distances.origin_distance_2 + :radius
                    )
                                
                    select
                        points.distance,
                        other.*
                    from
                        public.node other
                        join points on other.id = points.id
                    where
                        points.distance < :radius
                    order by
                        points.distance
                    """,
            nativeQuery = true
    )
    Collection<Node> findDistance(
            double lat,
            double lon,
            double radius
    );
}
