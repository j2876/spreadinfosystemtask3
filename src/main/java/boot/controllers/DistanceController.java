package boot.controllers;
import boot.DTOmodels.NodeDTO;
import boot.DTOmodels.TagDTO;
import boot.DTOmodels.UserDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import boot.repositories.NodeRepository;

@RestController
public class DistanceController {

    private final NodeRepository repository;

    public DistanceController(NodeRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/get_distance")
    List<NodeDTO> getDistance(
            @RequestParam double lat,
            @RequestParam double lon,
            @RequestParam double radius
    ) {

        return repository.findDistance(lat, lon, radius).stream()
                .map(x -> {
                    NodeDTO result = new NodeDTO();

                    UserDTO user = new UserDTO();
                    user.setName(x.getUser().getName());

                    List<TagDTO> tags = new ArrayList<>();
                    for (var tag :
                            x.getTags()) {
                        TagDTO tagDTO = new TagDTO();
                        tagDTO.setK(tag.getKey());
                        tagDTO.setV(tag.getValue());
                        tags.add(tagDTO);
                    }

                    result.setChangeSet(x.getChangeSet());
                    result.setVersion(x.getVersion());
                    result.setLatitude(x.getLat());
                    result.setLongitude(x.getLon());
                    result.setUser(user);
                    result.setTags(tags);
                    return result;
                })
                .collect(Collectors.toList());
    }
}
