package boot.DTOmodels;
import lombok.Data;

import java.util.Collection;
import java.util.Date;

@Data
public class NodeDTO {
    long id;

    long version;

    Date timestamp;

    UserDTO user;

    long changeSet;

    double latitude;

    double longitude;

    Collection<TagDTO> tags;
}

