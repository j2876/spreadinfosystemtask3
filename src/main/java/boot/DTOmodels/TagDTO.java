package boot.DTOmodels;

import lombok.Data;

@Data
public class TagDTO {
    String k;

    String v;
}
