package boot.DTOmodels;

import lombok.Data;

@Data
public class UserDTO {
    long id;

    String name;
}
